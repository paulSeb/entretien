
import { ApolloServer, gql } from "apollo-server-micro";
import markets from "./decp.json";

const typeDefs = gql`
  type Market {
    id: ID
    objet: String
    montant: Float
  }

  type Markets {
    marches: [Market]
  }

  type Query {
    markets: Markets
  }
`;

const resolvers = {
  Query: {
    markets: () => markets,
  },
};

const server = new ApolloServer({ typeDefs, resolvers });

const handler = server.createHandler({ path: "/api/graphql-data" });

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
