import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import { DataGrid } from "@material-ui/data-grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import Alert from "@material-ui/lab/Alert";
import { v4 as uuidv4 } from "uuid";

const GET_MARKET_LIST = gql`
  query {
    markets {
      marches {
        id
        objet
        montant
      }
    }
  }
`;

const MarketList = () => {
  const { error, loading, data } = useQuery(GET_MARKET_LIST);
  // if (loading) return <CircularProgress />;
  if (error) return <Alert severity="error">{error}</Alert>;

  const columns = [
    { field: "id", hide: true },
    { field: "objet", flex: 1, headerName: "Market" },
    { field: "montant", flex: 1, headerName: "Value (€)" },
  ];
  const rows = data
    ? data.markets.marches.map((marche) => {
        // TODO: dataset has missing or duplicates id
        marche.id = uuidv4();
        return marche;
      })
    : [];
  const infos = { columns, rows };
  return (
    <div style={{ height: 400 }}>
      <DataGrid autoPageSize pagination {...infos} loading={loading} />
    </div>
  );
};

export default MarketList;
