import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const DashboardCard = (props) => {
  const classes = makeStyles({
    root: {
      minWidth: 275,
    },
    title: {
      fontSize: 14,
    },
  });

  return (
    <Grid container spacing={3}>
      <Grid item xs={6}>
        <Card className={classes.root}>
          <CardContent>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
            >
              Public markets
            </Typography>
            <Typography variant="h5" component="h2">
              {props.title}
            </Typography>
            {props.children}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default DashboardCard;
