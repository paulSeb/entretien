import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import { DashboardCard } from "../DashboardCard";
import CircularProgress from "@material-ui/core/CircularProgress";
import Alert from "@material-ui/lab/Alert";
import Typography from "@material-ui/core/Typography";

const GET_MARKET_DETAILS = gql`
  query {
    markets {
      marches {
        montant
      }
    }
  }
`;

const MarketInfo = () => {
  const { loading, error, data } = useQuery(GET_MARKET_DETAILS);

  let results;
  if (data) {
    results = data.markets.marches.reduce(
      (current, { montant }) => {
        if (montant) {
          current.sum = current.sum + montant;
          current.counted++;
        }
        return current;
      },
      { sum: 0, counted: 0 }
    );
  }

  return (
    <>
      <DashboardCard title="TOTAL">
        {loading ? (
          <CircularProgress />
        ) : error ? (
          <Alert severity="error">{error}</Alert>
        ) : (
          <Typography variant="body2" component="p">
            {parseFloat(results.sum).toFixed(2)} €
          </Typography>
        )}
      </DashboardCard>
      <DashboardCard title="AVERAGE">
        {loading ? (
          <CircularProgress />
        ) : error ? (
          <Alert severity="error">{error}</Alert>
        ) : (
          <Typography variant="body2" component="p">
            {parseFloat(results.sum / results.counted).toFixed(2)} €
          </Typography>
        )}
      </DashboardCard>
    </>
  );
};

export default MarketInfo;
